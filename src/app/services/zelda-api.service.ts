import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { environment } from 'src/environments/environment';
import { Creature } from '../models/Creature';
import { EquipmentItem } from '../models/Equipment';
import { Item } from '../models/Item';
import { Material } from '../models/Material';
import { Monster } from '../models/Monster';
import { TreasureItem } from '../models/Treasure';

interface CreaturesResponse {
  data: { 
    food: {
      category: string,
      common_locations: string[],
      cooking_effect: string,
      description: string,
      hearts_recovered: number,
      id: number,
      image: string,
      name: string
    }[],
    non_food: {
      category: string,
      common_locations: string[],
      description: string,
      drops: string[],
      id: number,
      image: string,
      name: string
    }[]
  }
}

interface EquipmentResponse {
  data: {
    attack: number,
    category: string,
    common_locations: string[],
    defense: number,
    description: string,
    id: number,
    image: string,
    name: string
  }[]
}

interface MaterialsResponse {
  data: {
    category: string,
    common_locations: string[],
    cooking_effect: string,
    description: string,
    hearts_recovered: number,
    id: number,
    image: string,
    name: string
  }[]
}

interface MonstersResponse {
  data: {
    category: string,
    common_locations: string[],
    description: string,
    drops: string[],
    id: number,
    image: string,
    name: string
  }[]
}

interface TreaureResponse {
  data: {
    category: string,
    common_locations: string[],
    description: string,
    drops: string[],
    id: number,
    image: string,
    name: string
  }[]
}

const localStorageCreatures = 'creatures';
const localStorageEquipment = 'equipment';
const localStorageMaterials = 'materials';
const localStorageMonsters = 'monsters';
const localStorageTreasure = 'treasure';

@Injectable({
  providedIn: 'root'
})
export class ZeldaApiService {
  private _creatures: Creature[] = [];
  private _equipment: EquipmentItem[] = [];
  private _materials: Material[] = [];
  private _monsters: Monster[] = [];
  private _treasure: TreasureItem[] = [];
  private _error: string = '';

  constructor(private readonly http: HttpClient) { };

  capitalize(words: string): string {
    let tempWords = words.split(' ');
    for (let i = 0; i < tempWords.length; i++) {
      tempWords[i] = tempWords[i].charAt(0).toUpperCase() + tempWords[i].substr(1);
    }
    return tempWords.join(' ');
  }

  sort<Type extends Item>(array: Type[]): Type[] {
    const mapped = array.map((value, index) => {
      return {
        index, value
      };
    });

    mapped.sort((a, b) => {
      if (a.value.name > b.value.name) {
        return 1;
      }
      if (a.value.name < b.value.name) {
        return -1;
      }
      return 0;
    });

    return mapped.map(value => array[value.index]);
  }

  fetchCreatures(): void {
    const creaturesFromStorage = localStorage.getItem(localStorageCreatures);
    if (creaturesFromStorage != null) {
      this._creatures = JSON.parse(creaturesFromStorage);
    } else {
      this.http.get<CreaturesResponse>(environment.zeldaApi + 'category/creatures')
        .subscribe(
          (response: CreaturesResponse) => {
            response.data.food.forEach(result => {
              const newCreature : Creature = {
                commonLocations: result.common_locations,
                cookingEffect: result.cooking_effect,
                description: result.description,
                drops: [],
                heartsRecovered: result.hearts_recovered,
                id: result.id,
                image: result.image,
                name: this.capitalize(result.name),
                type: "Food"
              };
              this._creatures.push(newCreature);
            });

            response.data.non_food.forEach(result => {
              const newCreature : Creature = {
                commonLocations: result.common_locations,
                cookingEffect: '',
                description: result.description,
                drops: result.drops,
                heartsRecovered: 0,
                id: result.id,
                image: result.image,
                name: this.capitalize(result.name),
                type: "NonFood"
              };
              this._creatures.push(newCreature);
            });
            this._creatures = this.sort(this._creatures);
            localStorage.setItem(localStorageCreatures, JSON.stringify(this._creatures));
          },
          (error: HttpErrorResponse) => {
            this._error = error.message;
          }
        );
    }
  }

  fetchEquipment(): void {
    const equipmentFromStorage = localStorage.getItem(localStorageEquipment);
    if (equipmentFromStorage != null) {
      this._equipment = JSON.parse(equipmentFromStorage);
    } else {
      this.http.get<EquipmentResponse>(environment.zeldaApi + 'category/equipment')
        .subscribe(
          (response: EquipmentResponse) => {
            response.data.forEach(result => {
              const newEquipmentItem : EquipmentItem = {
                attack: result.attack,
                commonLocations: result.common_locations,
                defense: result.defense,
                description: result.description,
                id: result.id,
                image: result.image,
                name: this.capitalize(result.name),
              };
              this._equipment.push(newEquipmentItem);
            });
            this._equipment = this.sort(this._equipment);
            localStorage.setItem(localStorageEquipment, JSON.stringify(this._equipment));
          },
          (error: HttpErrorResponse) => {
            this._error = error.message;
          }
        );
    }
  }

  fetchMaterials(): void {
    const materialsFromStorage = localStorage.getItem(localStorageMaterials);
    if (materialsFromStorage != null) {
      this._materials = JSON.parse(materialsFromStorage);
    } else {
      this.http.get<MaterialsResponse>(environment.zeldaApi + 'category/materials')
        .subscribe(
          (response: MaterialsResponse) => {
            response.data.forEach(result => {
              const newMaterial : Material = {
                commonLocations: result.common_locations,
                cookingEffect: result.cooking_effect,
                description: result.description,
                heartsRecovered: result.hearts_recovered,
                id: result.id,
                image: result.image,
                name: this.capitalize(result.name),
              };
              this._materials.push(newMaterial);
            });
            this._materials = this.sort(this._materials);
            localStorage.setItem(localStorageMaterials, JSON.stringify(this._materials));
          },
          (error: HttpErrorResponse) => {
            this._error = error.message;
          }
        );
    }
  }

  fetchMonsters(): void {
    const monstersFromStorage = localStorage.getItem(localStorageMonsters);
    if (monstersFromStorage != null) {
      this._monsters = JSON.parse(monstersFromStorage);
    } else {
      this.http.get<MonstersResponse>(environment.zeldaApi + 'category/monsters')
        .subscribe(
          (response: MonstersResponse) => {
            response.data.forEach(result => {
              const newMonster : Monster = {
                commonLocations: result.common_locations,
                description: result.description,
                drops: result.drops,
                id: result.id,
                image: result.image,
                name: this.capitalize(result.name),
              };
              this._monsters.push(newMonster);
            });
            this._monsters = this.sort(this._monsters);
            localStorage.setItem(localStorageMonsters, JSON.stringify(this._monsters));
          },
          (error: HttpErrorResponse) => {
            this._error = error.message;
          }
        );
    }
  }

  fetchTreasure(): void {
    const treasureFromStorage = localStorage.getItem(localStorageTreasure);
    if (treasureFromStorage != null) {
      this._treasure = JSON.parse(treasureFromStorage);
    } else {
      this.http.get<TreaureResponse>(environment.zeldaApi + 'category/treasure')
        .subscribe(
          (response: TreaureResponse) => {
            response.data.forEach(result => {
              const newTreasureItem : TreasureItem = {
                commonLocations: result.common_locations,
                description: result.description,
                drops: result.drops,
                id: result.id,
                image: result.image,
                name: this.capitalize(result.name),
              };
              this._treasure.push(newTreasureItem);
            });
            this._treasure = this.sort(this._treasure);
            localStorage.setItem(localStorageTreasure, JSON.stringify(this._treasure));
          },
          (error: HttpErrorResponse) => {
            this._error = error.message;
          }
        );
    }
  }

  get creatures(): Creature[] {
    return this._creatures;
  }

  get equipment(): EquipmentItem[] {
    return this._equipment;
  }

  get materials(): Material[] {
    return this._materials;
  }

  get monsters(): Monster[] {
    return this._monsters;
  }

  get treasure(): TreasureItem[] {
    return this._treasure;
  }
}
