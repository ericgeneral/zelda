import { TestBed } from '@angular/core/testing';

import { ZeldaApiService } from './zelda-api.service';

describe('ZeldaApiService', () => {
  let service: ZeldaApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ZeldaApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
