import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Material } from 'src/app/models/Material';
import { ZeldaApiService } from 'src/app/services/zelda-api.service';

@Component({
  selector: 'app-materials',
  templateUrl: './materials.component.html',
  styleUrls: ['./materials.component.css']
})
export class MaterialsComponent implements OnInit {
  pageIndex: number = 0;
  pageSize: number = 15;
  private filteredMaterials: Material[] = [];
  materialsForm = new FormGroup({
    filterText: new FormControl('')
  });
  currentMaterial!: Material;

  constructor(private readonly zeldaApiService: ZeldaApiService) { }

  ngOnInit(): void {
    if (this.zeldaApiService.materials.length === 0) {
      this.zeldaApiService.fetchMaterials();
    }
  }

  setFilteredCatalog(): void {
    // Filter by text
    if (this.materialsForm.controls.filterText.value.length > 0) {
      this.filteredMaterials = this.materials.filter(
        material => material.name.toLowerCase()
          .includes(this.materialsForm.controls.filterText.value.toLowerCase()));
    }

    this.pageIndex = 0;
  }

  filterChanged(): void {
    this.setFilteredCatalog();
  }

  get materials(): Material[] {
    return this.zeldaApiService.materials;
  }

  get page(): Material[] {
    if (this.isFiltered())
      return this.filteredMaterials.slice(this.pageIndex * this.pageSize, this.pageIndex * this.pageSize + this.pageSize);
    else
      return this.materials.slice(this.pageIndex * this.pageSize, this.pageIndex * this.pageSize + this.pageSize);
  }

  goBack(): void {
    if (this.pageIndex > 0)
      this.pageIndex--;
  }

  goForward(): void {
    if (this.isFiltered()){
      if (this.pageIndex * this.pageSize + this.pageSize < this.filteredMaterials.length)
        this.pageIndex++;
    } else {
      if (this.pageIndex * this.pageSize + this.pageSize < this.materials.length)
        this.pageIndex++;
    }
  }

  isFiltered(): boolean {
    if (this.materialsForm.controls.filterText.value.length === 0)
      return false;
    return true;
  }

  handleClick(material: Material) {
    this.currentMaterial = material;
  }
}
