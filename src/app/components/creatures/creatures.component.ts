import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Creature } from 'src/app/models/Creature';
import { ZeldaApiService } from 'src/app/services/zelda-api.service';

@Component({
  selector: 'app-creatures',
  templateUrl: './creatures.component.html',
  styleUrls: ['./creatures.component.css']
})
export class CreaturesComponent implements OnInit {
  pageIndex: number = 0;
  pageSize: number = 15;
  private filteredCreatures: Creature[] = [];
  creaturesForm = new FormGroup({
    isFoodChecked: new FormControl(true),
    isNonFoodChecked: new FormControl(true),
    filterText: new FormControl('')
  });
  currentCreature!: Creature;

  constructor(private readonly zeldaApiService: ZeldaApiService) { }

  ngOnInit(): void {
    if (this.zeldaApiService.creatures.length === 0) {
      this.zeldaApiService.fetchCreatures();
    }
  }

  setFilteredCatalog(): void {
    let tempCreatures: Creature[] = [];
    // Filter by checkboxes
    if (this.creaturesForm.controls.isFoodChecked.value) {
      tempCreatures.push(...this.zeldaApiService.creatures.filter(creature => creature.type === "Food"));
    }
    if (this.creaturesForm.controls.isNonFoodChecked.value) {
      tempCreatures.push(...this.zeldaApiService.creatures.filter(creature => creature.type === "NonFood"));
    }
    // Filter by text
    if (this.creaturesForm.controls.filterText.value.length > 0) {
      this.filteredCreatures = tempCreatures.filter(
        creature => creature.name.toLowerCase()
          .includes(this.creaturesForm.controls.filterText.value.toLowerCase()));
    } else {
      this.filteredCreatures = tempCreatures;
    }

    this.pageIndex = 0;
  }

  toggleFood(): void {
    this.setFilteredCatalog();
  }

  toggleNonFood(): void {
    this.setFilteredCatalog();
  }

  filterChanged(): void {
    this.setFilteredCatalog();
  }

  get creatures(): Creature[] {
    return this.zeldaApiService.creatures;
  }

  get page(): Creature[] {
    if (this.isFiltered())
      return this.filteredCreatures.slice(this.pageIndex * this.pageSize, this.pageIndex * this.pageSize + this.pageSize);
    else
      return this.creatures.slice(this.pageIndex * this.pageSize, this.pageIndex * this.pageSize + this.pageSize);
  }

  goBack(): void {
    if (this.pageIndex > 0)
      this.pageIndex--;
  }

  goForward(): void {
    if (this.isFiltered()){
      if (this.pageIndex * this.pageSize + this.pageSize < this.filteredCreatures.length)
        this.pageIndex++;
    } else {
      if (this.pageIndex * this.pageSize + this.pageSize < this.creatures.length)
        this.pageIndex++;
    }
  }

  isFiltered(): boolean {
    if (this.creaturesForm.controls.isFoodChecked.value 
      && this.creaturesForm.controls.isNonFoodChecked.value
      && this.creaturesForm.controls.filterText.value.length === 0)
      return false;
    return true;
  }

  handleClick(creature: Creature) {
    this.currentCreature = creature;
  }
}
