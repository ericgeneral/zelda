import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { TreasureItem } from 'src/app/models/Treasure';
import { ZeldaApiService } from 'src/app/services/zelda-api.service';

@Component({
  selector: 'app-treasure',
  templateUrl: './treasure.component.html',
  styleUrls: ['./treasure.component.css']
})
export class TreasureComponent implements OnInit {
  pageIndex: number = 0;
  pageSize: number = 15;
  private filteredTreasure: TreasureItem[] = [];
  treasureForm = new FormGroup({
    filterText: new FormControl('')
  });
  currentTreasureItem!: TreasureItem;

  constructor(private readonly zeldaApiService: ZeldaApiService) { }

  ngOnInit(): void {
    if (this.zeldaApiService.treasure.length === 0) {
      this.zeldaApiService.fetchTreasure();
    }
  }

  setFilteredCatalog(): void {
    // Filter by text
    if (this.treasureForm.controls.filterText.value.length > 0) {
      this.filteredTreasure = this.treasure.filter(
        treasureItem => treasureItem.name.toLowerCase()
          .includes(this.treasureForm.controls.filterText.value.toLowerCase()));
    }

    this.pageIndex = 0;
  }

  filterChanged(): void {
    this.setFilteredCatalog();
  }

  get treasure(): TreasureItem[] {
    return this.zeldaApiService.treasure;
  }

  get page(): TreasureItem[] {
    if (this.isFiltered())
      return this.filteredTreasure.slice(this.pageIndex * this.pageSize, this.pageIndex * this.pageSize + this.pageSize);
    else
      return this.treasure.slice(this.pageIndex * this.pageSize, this.pageIndex * this.pageSize + this.pageSize);
  }

  goBack(): void {
    if (this.pageIndex > 0)
      this.pageIndex--;
  }

  goForward(): void {
    if (this.isFiltered()){
      if (this.pageIndex * this.pageSize + this.pageSize < this.filteredTreasure.length)
        this.pageIndex++;
    } else {
      if (this.pageIndex * this.pageSize + this.pageSize < this.treasure.length)
        this.pageIndex++;
    }
  }

  isFiltered(): boolean {
    if (this.treasureForm.controls.filterText.value.length === 0)
      return false;
    return true;
  }

  handleClick(treasureItem: TreasureItem) {
    this.currentTreasureItem = treasureItem;
  }
}
