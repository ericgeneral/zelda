import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Monster } from 'src/app/models/Monster';
import { ZeldaApiService } from 'src/app/services/zelda-api.service';

@Component({
  selector: 'app-monsters',
  templateUrl: './monsters.component.html',
  styleUrls: ['./monsters.component.css']
})
export class MonstersComponent implements OnInit {
  pageIndex: number = 0;
  pageSize: number = 15;
  private filteredMonsters: Monster[] = [];
  monstersForm = new FormGroup({
    filterText: new FormControl('')
  });
  currentMonster!: Monster;

  constructor(private readonly zeldaApiService: ZeldaApiService) { }

  ngOnInit(): void {
    if (this.zeldaApiService.monsters.length === 0) {
      this.zeldaApiService.fetchMonsters();
    }
  }

  setFilteredCatalog(): void {
    // Filter by text
    if (this.monstersForm.controls.filterText.value.length > 0) {
      this.filteredMonsters = this.monsters.filter(
        monster => monster.name.toLowerCase()
          .includes(this.monstersForm.controls.filterText.value.toLowerCase()));
    }

    this.pageIndex = 0;
  }

  filterChanged(): void {
    this.setFilteredCatalog();
  }

  get monsters(): Monster[] {
    return this.zeldaApiService.monsters;
  }

  get page(): Monster[] {
    if (this.isFiltered())
      return this.filteredMonsters.slice(this.pageIndex * this.pageSize, this.pageIndex * this.pageSize + this.pageSize);
    else
      return this.monsters.slice(this.pageIndex * this.pageSize, this.pageIndex * this.pageSize + this.pageSize);
  }

  goBack(): void {
    if (this.pageIndex > 0)
      this.pageIndex--;
  }

  goForward(): void {
    if (this.isFiltered()){
      if (this.pageIndex * this.pageSize + this.pageSize < this.filteredMonsters.length)
        this.pageIndex++;
    } else {
      if (this.pageIndex * this.pageSize + this.pageSize < this.monsters.length)
        this.pageIndex++;
    }
  }

  isFiltered(): boolean {
    if (this.monstersForm.controls.filterText.value.length === 0)
      return false;
    return true;
  }

  handleClick(monster: Monster) {
    this.currentMonster = monster;
  }
}
