import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { EquipmentItem } from 'src/app/models/Equipment';
import { ZeldaApiService } from 'src/app/services/zelda-api.service';

@Component({
  selector: 'app-equipment',
  templateUrl: './equipment.component.html',
  styleUrls: ['./equipment.component.css']
})
export class EquipmentComponent implements OnInit {
  pageIndex: number = 0;
  pageSize: number = 15;
  private filteredEquipment: EquipmentItem[] = [];
  equipmentForm = new FormGroup({
    filterText: new FormControl('')
  });
  currentEquipmentItem!: EquipmentItem;

  constructor(private readonly zeldaApiService: ZeldaApiService) { }

  ngOnInit(): void {
    if (this.zeldaApiService.equipment.length === 0) {
      this.zeldaApiService.fetchEquipment();
    }
  }

  setFilteredCatalog(): void {
    // Filter by text
    if (this.equipmentForm.controls.filterText.value.length > 0) {
      this.filteredEquipment = this.equipment.filter(
        equipmentItem => equipmentItem.name.toLowerCase()
          .includes(this.equipmentForm.controls.filterText.value.toLowerCase()));
    }

    this.pageIndex = 0;
  }

  filterChanged(): void {
    this.setFilteredCatalog();
  }

  get equipment(): EquipmentItem[] {
    return this.zeldaApiService.equipment;
  }

  get page(): EquipmentItem[] {
    if (this.isFiltered())
      return this.filteredEquipment.slice(this.pageIndex * this.pageSize, this.pageIndex * this.pageSize + this.pageSize);
    else
      return this.equipment.slice(this.pageIndex * this.pageSize, this.pageIndex * this.pageSize + this.pageSize);
  }

  goBack(): void {
    if (this.pageIndex > 0)
      this.pageIndex--;
  }

  goForward(): void {
    if (this.isFiltered()){
      if (this.pageIndex * this.pageSize + this.pageSize < this.filteredEquipment.length)
        this.pageIndex++;
    } else {
      if (this.pageIndex * this.pageSize + this.pageSize < this.equipment.length)
        this.pageIndex++;
    }
  }

  isFiltered(): boolean {
    if (this.equipmentForm.controls.filterText.value.length === 0)
      return false;
    return true;
  }

  handleClick(equipmentItem: EquipmentItem) {
    this.currentEquipmentItem = equipmentItem;
  }
}
