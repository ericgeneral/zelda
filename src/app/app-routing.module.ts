import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreaturesComponent } from './components/creatures/creatures.component';
import { EquipmentComponent } from './components/equipment/equipment.component';
import { HomeComponent } from './components/home/home.component';
import { MaterialsComponent } from './components/materials/materials.component';
import { MonstersComponent } from './components/monsters/monsters.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { TreasureComponent } from './components/treasure/treasure.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'creatures', component: CreaturesComponent },
  { path: 'equipment', component: EquipmentComponent },
  { path: 'materials', component: MaterialsComponent },
  { path: 'monsters', component: MonstersComponent },
  { path: 'treasure', component: TreasureComponent },
  { path: '', pathMatch: 'full', redirectTo: '/home' },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
