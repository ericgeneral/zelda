import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { CreaturesComponent } from './components/creatures/creatures.component';
import { EquipmentComponent } from './components/equipment/equipment.component';
import { MaterialsComponent } from './components/materials/materials.component';
import { MonstersComponent } from './components/monsters/monsters.component';
import { TreasureComponent } from './components/treasure/treasure.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { HeadingComponent } from './components/heading/heading.component';
import { ItemComponent } from './components/item/item.component';
import { ItemDetailsComponent } from './components/item-details/item-details.component';
import { DetailsModalComponent } from './components/details-modal/details-modal.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CreaturesComponent,
    EquipmentComponent,
    MaterialsComponent,
    MonstersComponent,
    TreasureComponent,
    PageNotFoundComponent,
    HeadingComponent,
    ItemComponent,
    ItemDetailsComponent,
    DetailsModalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
