import { Item } from "./Item";

export interface Material extends Item {
  cookingEffect: string,
  heartsRecovered: number
}