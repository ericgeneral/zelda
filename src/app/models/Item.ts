export interface Item {
  commonLocations: string[],
  description: string,
  id: number,
  image: string,
  name: string,
}