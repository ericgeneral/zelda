import { Item } from "./Item";

export interface Monster extends Item {
  drops: string[]
}