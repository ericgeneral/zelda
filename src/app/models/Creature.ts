import { Item } from "./Item";

export interface Creature extends Item {
  cookingEffect: string,
  drops: string[],
  heartsRecovered: number,
  type: string
}