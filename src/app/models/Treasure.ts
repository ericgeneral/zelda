import { Item } from "./Item";

export interface TreasureItem extends Item {
  drops: string[]
}