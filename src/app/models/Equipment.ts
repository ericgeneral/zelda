import { Item } from "./Item";

export interface EquipmentItem extends Item {
  attack: number,
  defense: number
}